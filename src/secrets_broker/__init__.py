from flask_api import FlaskAPI
from flask import jsonify

# Local imports
from secrets_broker.vault import VaultSecret
from instance.config import app_config


def create_app(config_name):
    app = FlaskAPI(__name__, instance_relative_config=True)
    app.config.from_object(app_config[config_name])
    app.config.from_pyfile('config.py')
    vs = VaultSecret(app.config)

    @app.route('/api/v1/secret/<secret_name>', methods=['GET'])
    def get_secret(secret_name):
        secret = vs.retrieve(secret_name=secret_name)

        return jsonify(secret)

    return app
