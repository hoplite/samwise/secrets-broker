import hvac


class VaultSecret:

    def __init__(self, config):
        self.config = config
        self.client = hvac.Client(url=self.config['VAULT_ADDR'])

    def retrieve(self, secret_name='', version=1):
        secret = None
        self.client.auth_approle(self.config['VAULT_ROLE_ID'],
                                 self.config['VAULT_SECRET_ID'])

        if version in self.config['VAULT_VERSIONS']:
            self.path = '/'.join([self.config['VAULT_SECRETS_PATH'],
                                  secret_name])
            if version == 1:
                secret = self.client.kv.v1.read_secret(path=self.path)
            else:
                resp = self.client.kv.v2.read_secret_versions(path=self.path)
                secret = resp['data']['data']

        return secret
