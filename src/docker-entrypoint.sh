#!/usr/bin/env bash
export PYTHONPATH="/app"

if [ "$1" == "run" ]; then
  gunicorn run:app --bind 0.0.0.0:5000 --timeout=6000 --workers=$API_WORKERS
elif [ "$1" == "test" ]; then
  pytest -v -s --cov-report term-missing --cov=secrets_broker --basetemp={envtmpdir} tests
else
  exec $@
fi

