import json

from unittest import mock

# Local imports
from secrets_broker import create_app


class TestAPI(object):

    @mock.patch('secrets_broker.vault.hvac.Client')
    def test_get_secret(self, mock_vault):
        secret_data = {'mysecret': 'test123'}

        vault_payload = {'request_id': '35055a27-00c6-37b9-00b5-9abc948e529b',
                         'lease_id': '',
                         'renewable': False,
                         'lease_duration': 2764800,
                         'data': {
                             'mysecret': "test123"
                         },
                         'wrap_info': None,
                         'warnings': None,
                         'auth': None}
        mock_vault().kv.v1.read_secret.return_value = vault_payload

        self.app = create_app(config_name="development")
        self.client = self.app.test_client
        app_response = self.client().get('/api/v1/secret/test')
        app_response_dict = json.loads(app_response.data)

        assert app_response_dict['data'] == secret_data
        assert app_response.status_code == 200

