import mock
import os
import yaml

from instance.config import app_config
from secrets_broker.vault import VaultSecret


class TestVaultSecret(object):

    @staticmethod
    def config_from_object(obj):
        """
        This function receives a config object, and returns a config dictionary.
        It was taken from the flask project to replicate their config handling.

        :param obj: Object class passed in with config data
        :return config: Dictionary of configuration settings
        """

        config = {}
        for key in dir(obj):
            if key.isupper():
                config[key] = getattr(obj, key)

        return config

    @mock.patch('secrets_broker.vault.hvac.Client')
    def test_vault_v1_retrieve(self, mock_vault):
        """
        Ensure the retrieve method is returning the correct secret data.
        """
        config = self.config_from_object(app_config['development'])
        payload = {'request_id': '35055a27-00c6-37b9-00b5-9abc948e529b',
                   'lease_id': '',
                   'renewable': False,
                   'lease_duration': 2764800,
                   'data': {
                       'mysecret': "test123"
                   },
                   'wrap_info': None,
                   'warnings': None,
                   'auth': None}

        mock_vault().kv.v1.read_secret.return_value = payload

        v = VaultSecret(config)
        s = v.retrieve(secret_name='test', version=1)
        mock_vault().kv.v1.read_secret.assert_called_with(path='mysecrets/test')
        assert s['data']['mysecret'] == 'test123'
