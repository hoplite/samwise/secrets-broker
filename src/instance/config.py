import os


class Config(object):
    """Parent configuration class."""
    DEBUG = False
    CSRF_ENABLED = True
    VAULT_ADDR = os.getenv('VAULT_ADDR')
    VAULT_VERSIONS = [1, 2]

class DevelopmentConfig(Config):
    """Configurations for Development."""

    DEBUG = True
    VAULT_ADDR = 'https://127.0.0.1:8200'
    VAULT_SECRETS_PATH = 'mysecrets' 
    VAULT_ROLE_ID = 'abc123'
    VAULT_SECRET_ID = 'def456'

class TestingConfig(Config):
    """Configurations for Testing."""

    DEBUG = True
    TESTING = True
    VAULT_SECRETS_PATH = os.getenv('VAULT_SECRETS_PATH')
    VAULT_ROLE_ID = os.getenv('VAULT_ROLE_ID')
    VAULT_SECRET_ID = os.getenv('VAULT_SECRET_ID')

class ProductionConfig(Config):
    """Configurations for Production."""

    DEBUG = False
    TESTING = False
    VAULT_SECRETS_PATH = os.getenv('VAULT_SECRETS_PATH')
    VAULT_ROLE_ID = os.getenv('VAULT_ROLE_ID')
    VAULT_SECRET_ID = os.getenv('VAULT_SECRET_ID')


app_config = {
    'development': DevelopmentConfig,
    'testing': TestingConfig,
    'production': ProductionConfig
}
